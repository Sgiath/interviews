"""Reference solution of Palindrome exercise.

Author: Sgiath <FilipVavera@sgiath.dev>
"""
from re import sub


def standard_string_reverse(msg: str) -> bool:
    """Reverse string with reverse() function- perfectly acceptable solution.

    Using re.sub() for replacing non-letter values with Regex
    Using string.casefold() method instead of string.lower() because of its universality
    (though string.lower() is OK too)
    """
    msg = sub(r"\W", "", msg.casefold())

    return msg == "".join(reversed(msg))


def clever_string_reverse(msg: str) -> bool:
    """Reverse string using slicing.

    This solution shows great knowledge about Python
    """
    msg = sub(r"\W", "", msg.casefold())

    return msg == msg[::-1]
