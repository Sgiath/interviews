"""Palindrome exercise"""


def is_palindrome(msg: str) -> bool:
    """Is the `msg` palindrome or not?
    
    Arguments:
        msg (str): message to inspect for palindrome

    Returns:
        bool: is `msg` palindrome or not
    """
    # TODO: your implementation
    pass
