import { isPalindrome } from "./exercise";

test("basic odd palindrome", () => {
  expect(isPalindrome("level")).toBeTruthy();
});

test("basic even palindrome", () => {
  expect(isPalindrome("levvel")).toBeTruthy();
});

test("basic odd not palindrome", () => {
  expect(isPalindrome("leves")).toBeFalsy();
});

test("basic even not palindrome", () => {
  expect(isPalindrome("levwel")).toBeFalsy();
});

test("palindrome with spaces", () => {
  expect(isPalindrome("lev el")).toBeTruthy();
});

test("not palindrome with spaces", () => {
  expect(isPalindrome("lev l")).toBeFalsy();
});

test("palindrome sentence", () => {
  expect(isPalindrome("A car, a man, a maraca?")).toBeTruthy();
});
