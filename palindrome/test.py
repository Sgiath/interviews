"""Unittests for Palindrome exercise.

Author: Sgiath <FilipVavera@sgiath.dev>
"""
import unittest

from .exercise import is_palindrome


class TestIsPalindrome(unittest.TestCase):
    """Test is_palindrome function."""

    def test_basic_odd_palindrome(self) -> None:
        """Basic palindrome with odd number of characters."""
        self.assertTrue(is_palindrome("level"))

    def test_basic_even_palindrome(self) -> None:
        """Basic palindrome with even number of characters."""
        self.assertTrue(is_palindrome("levvel"))

    def test_basic_odd_not_palindrome(self) -> None:
        """Basic string with odd number of characters which is not palindrome."""
        self.assertFalse(is_palindrome("leves"))

    def test_basic_even_not_palindrome(self) -> None:
        """Basic string with even number of characters which is not palindrome."""
        self.assertFalse(is_palindrome("levwel"))

    def test_palindrom_with_spaces(self) -> None:
        """Palindrome with spaces."""
        self.assertTrue(is_palindrome("lev el"))

    def test_not_palindrome_with_spaces(self) -> None:
        """String with spaces which is not palindrome."""
        self.assertFalse(is_palindrome("lev l"))

    def test_palindrome_sentence(self) -> None:
        """Complex palindrome with spaces and puntation."""
        self.assertTrue(is_palindrome("A car, a man, a maraca?"))


if __name__ == "__main__":
    unittest.main()
