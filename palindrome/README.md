# Palindrome

**Difficulty:** Easy

Write a simple function that returns a boolean indicator whether or not a
string is a palindrome.

**Palindrome** is string which reads the same from backwards ignoring all
characters except `a-zA-Z`. Lower and upper cased letters reads the same.

**Hint:** regex for every character not in `a-zA-Z` is `/\W/g`.
