defmodule Interviews.Palindrome.Solution do
  @moduledoc false

  @doc """
  Remove non-alpha characters, downcase and run choosen solution
  """
  @spec is_palindrome(string :: String.t()) :: boolean()
  def is_palindrome(string) when is_binary(string) do
    string
    |> String.replace(~r/\W/, "")
    |> String.downcase()
    |> reverse()

    # |> recursive()
  end

  @doc """
  Reverse and compare
  """
  @spec reverse(string :: String.t()) :: boolean()
  def reverse(string) when is_binary(string) do
    string = String.to_charlist(string)

    Enum.reverse(string) == string
  end

  @doc """
  Recursive pattern matching solution
  """
  @spec recursive(string :: String.t()) :: boolean()
  def recursive(string) when is_binary(string) do
    case String.length(string) do
      0 ->
        true

      1 ->
        true

      length ->
        if String.first(string) == String.last(string) do
          recursive(String.slice(string, 1..(length - 2)))
        else
          false
        end
    end
  end
end
