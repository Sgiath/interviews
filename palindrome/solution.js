/**
 * Not a great solution but works. Everybody should be able to recognize it could be
 * optimized and do it.
 */
export function naiveForLoop(str) {
  // Standardize string
  str = str.replace(/\W/g, "").toLowerCase();

  for (let i = 0; i < str.length; i++) {
    const opposite = str.length - (i + 1);
    if (str[i] !== str[opposite]) {
      return false;
    }
  }

  return true;
}

/**
 * Smart solution
 */
export function reverseArray(str) {
  // Standardize string
  str = str.replace(/\W/g, "").toLowerCase();

  const reversed = str
    .split("")
    .reverse()
    .join("");

  return str === reversed;
}
