defmodule Interviews.Palindrome.Test do
  use ExUnit.Case, async: true

  doctest Interviews.Palindrome.Exercise
  doctest Interviews.Palindrome.Solution

  alias Interviews.Palindrome.Solution, as: Palindrome

  test "basic palindrome with odd number of characters" do
    assert Palindrome.solve("level")
  end

  test "basic palindrome with even number of characters" do
    assert Palindrome.solve("levvel")
  end

  test "basic string with odd number of characters which is not palindrome" do
    assert Palindrome.solve("leves") == false
  end

  test "basic string with even number of characters which is not palindrome" do
    assert Palindrome.solve("levwel") == false
  end

  test "palindrome with spaces" do
    assert Palindrome.solve("lev el")
  end

  test "string with spaces which is not palindrome" do
    assert Palindrome.solve("lev wel") == false
  end

  test "complex palindrome with spaces and puntation" do
    assert Palindrome.solve("A car, a man, a maraca?")
  end
end
