defmodule Interviews.Palindrome.Exercise do
  @moduledoc """
  Palindrome exercise
  """

  @doc """
  Your solution to palindrome problem
  """
  @spec is_palindrome(input :: String.t()) :: boolean()
  def is_palindrome(input) when is_binary(input) do
    # TODO: your implementation
  end
end
