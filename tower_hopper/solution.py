"""Tower Hopper solution.

Author: Sgiath <FilipVavera@sgiath.dev>
"""
from typing import List
from functools import lru_cache


def graph(arr: List[int]) -> bool:
    """Construct a graph from the towers and traverse the graph to find if it is possible to get
    to the node which represents outside of an array
    """
    pass


def dynamic(arr: List[int]) -> bool:
    """Dynamic solution.

    Recursively calculate if array is hoppable from given index. Use dynamic programming to
    memoize results.
    """
    return _dynamic_helper(arr, 0)


def _dynamic_helper(arr: List[int], index: int) -> bool:
    # on 0 field I am stuck
    if arr[index] == 0:
        return False

    # outside of an array condition is fulfilled
    if index + arr[index] >= len(arr):
        return True

    result = False
    for i in range(index + 1, index + arr[index] + 1):
        result = result or _dynamic_helper(arr, i)

    return result


def simple(arr: List[int]) -> bool:
    """Based on calculating optimal step for each index."""
    # calculate optimal step for index 0
    i = optimal_step(arr, 0)

    # while I am not outside of the array repeat
    while i < len(arr):
        # if my optimal step takes me to 0 I a stuck so array is not hoppable
        if arr[i] == 0:
            return False

        # otherwise calculate next optimal step
        i = optimal_step(arr, i)

    # if I am out of the while cycle I am outside of an array hence hoppable
    return True


def optimal_step(arr: List[int], index: int) -> int:
    """Calculate next optimal step (which takes me farthest)."""
    return max([i + arr[i] for i in range(index + 1, index + arr[index] + 1)])
