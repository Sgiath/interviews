# Tower Hopper

You are given an array of integers where each index represents one tower and value of
that index specifies how far you can "jump" from that tower.

For example `[4, 2, 0, 0, 2, 0]` means that from index `0` you can jump to indexes
`1-4`, from index `1` you can jump to indexes `2 and 3`, from indexes `2`, `3` and `5`
you cannot jump and from index `4` you can jump to index `5` and outside of the array.

    | #           |
    | #           |
    | # #     #   |
    | # #     #   |
    ---------------
    | 0 1 2 3 4 5 |
    ---------------

Your task is to write a function `is_hoppable()` which determine if in given
configurations of towers you can jump outside of the array or not starting from index
`0`.
