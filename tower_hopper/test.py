"""Tower Hopper tests.

TODO: Add more tests

Author: Sgiath <FilipVavera@sgiath.dev>
"""
import unittest

from .exercise import is_hoppable


class TestIsHoppable(unittest.TestCase):
    """Test is_hoppable function."""

    def test_basic_example(self) -> None:
        """Basic example from README."""
        self.assertTrue(is_hoppable([4, 2, 0, 0, 2, 0]))

    def test_simple_not_hoppable(self) -> None:
        """Simple not hoppable field."""
        self.assertFalse(is_hoppable([1, 0]))


if __name__ == "__main__":
    unittest.main()
