"""Tower Hopper exercise"""
from typing import List


def is_hoppable(arr: List[int]) -> bool:
    """Return boolean indicator if given field is hoppable.
    
    Arguments:
        arr: Array with tower heights

    Returns:
        bool: Is the array of towers hoppable or not?
    """
    # TODO: your implementation
    pass
