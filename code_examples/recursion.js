function func1(array, index) {
  if (index == 0) {
    return array[0];
  }
  return array[index] + func1(array, index - 1);
}

function func2(array, index, result = 0) {
  if (index == 0) {
    return result + array[0];
  }
  return func2(array, index - 1, result + array[index]);
}

const arr = [1, 2, 3];
const i = arr.length - 1;

console.log(func1(arr, i));
console.log(func2(arr, i));
