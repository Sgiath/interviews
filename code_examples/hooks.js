import { useState, useEffect } from "react";

const useWindowSize = () => {
  const getSize = () => ({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const [size, setSize] = useState(getSize);

  useEffect(() => {
    const handleResize = () => setSize(getSize());

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return size;
};

export default useWindowSize;

/**
 * 1. Is that necessary to call your function useWindowSize , what about just getWindowSize?
 * 2. Will it work if we remove [] argument from useEffect?
 * 3. How React knows when to re-render App component if we handle window resizing in useWindowSize?
 */
