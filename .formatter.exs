[
  inputs: ["{coordinates,crypto,game_of_life,palindrome,rand_sort,tower_hopper}/**/*.{ex,exs}"],
  rename_deprecated_at: "1.10.0"
]
