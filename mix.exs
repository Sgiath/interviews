defmodule Interviews.MixProject do
  use Mix.Project

  def project do
    [
      app: :interviews,
      version: "0.1.0",
      elixir: "~> 1.10",
      deps: deps(),
      elixirc_paths: paths(),
      test_paths: ["."],
      test_pattern: "test.exs"
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  def paths do
    [
      "coordinates",
      "palindrome"
    ]
  end

  def deps do
    [
      {:credo, "~> 1.3", only: [:dev, :test], runtime: false}
    ]
  end
end
