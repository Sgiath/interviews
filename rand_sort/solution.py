"""Rand Sort exercise solution.

Author: Sgiath <FilipVavera@sgiath.dev>
"""
from math import floor
from random import random
from typing import List


def new_random_array(arr: List[int]) -> List[int]:
    """Random array.

    Generate new random array with same length as original array and sort the array based on
    the randomly generated.

    Time complexity:
        O(n * log(n)) due to a sorting (assuming quick-sort otherwise even worse
        complexity)

    Space complexity:
        O(n) due to a new random array
    """
    # new array with random values
    tmp = map(lambda _: random(), arr)

    # sort by random array
    sorted_array = sorted(zip(tmp, arr))

    # unpack from zipped arrays
    return [x for _, x in sorted_array]


def new_random_array_oneliner(arr: List[int]) -> List[int]:
    """Previous function as one-liner."""
    return [x for _, x in sorted(zip(map(lambda _: random(), arr), arr))]


def pick_and_swap(arr: List[int]) -> List[int]:
    """Pick and swap.

    Go through array from back and on each index randomly pick what value should be
    on that index (it's index) and then swap the values

    Time complexity: O(n)
    Space complexity: O(0) (or O(1) if tmp value is used)
    """
    for i in reversed(range(1, len(arr))):
        # compute randomly which index should be on i index
        j = floor(random() * (i + 1))

        # swap the indexes
        arr[i], arr[j] = arr[j], arr[i]

    return arr
