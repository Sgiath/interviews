"""Rand Sort exercise"""
from math import floor
from random import random, randint
from typing import List


def random_sort(arr: List[int]) -> List[int]:
    """Sort given array randomly."""
    # TODO: your implementation
    pass


def test() -> None:
    """Basic tests - just print both arrays to see how it is sorted."""
    # Generate new random array
    arr = [randint(0, 100) for _ in range(10)]

    # Compare original and randomly sorted array
    print("Input :", arr)
    print("Output:", random_sort(arr))
    print()


if __name__ == "__main__":
    # Run 10 random tests
    for _ in range(10):
        test()
