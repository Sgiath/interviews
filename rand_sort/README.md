# Random sort

**Difficulty:** Easy

Sort the array randomly given the function `random()` which will return random value
between 0 and 1 and function `floor()` which will return the biggest integer lower or
equal the parameter.

_Bonus:_ solve it in `O(n)` time
