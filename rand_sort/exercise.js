function randSort(arr) {
  // TODO: your implementation
  return arr;
}

// Run 10 random tests
for (let i = 0; i < 10; i++) {
  // Generate new random array
  const arr = Array.from({ length: 10 }, () => Math.floor(Math.random() * 100));

  // Compare original and randomly sorted array
  console.log("Input :", arr);
  console.log("Output:", randSort(arr));
  console.log();
}
