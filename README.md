# Interview questions

## Personal

- What is your role in the current team? Is it something that suits you or do you feel you could
  do better on different role?
- What is your motivation for programming? What gets you out of the bed every morning and on your
  way to work?

## High-level/theoretical

### Junior

- What programming paradigms do you know? Can you describe them? Why they emerged and what problem
  do they solve?
- What is the main difference between _declarative_ and _imperative_ approach to programming? Can
  you show some example?
- What are the pros and cons of Functional languages? How is it defined?
- How do you keep track of different versions and history of your code? Do you know Git? What is
  your preferred Git workflow?
- Did you heard about _microservices_? Do you know how they works? Why are they so popular? What
  is the base premise? What problems can they solve?

### Senior

- Describe the project you are working on right now from architectural point of view. How the
  development workflow looks like? Is it monolith or are there multiple parts? How the deployment
  process looks like? Do you use Continuous Integration and Continuous Delivery (CI/CD)? How the
  testing looks like? Do you use unit tests, functional tests, manual testing, QA department?
  Do you know and use Test Driven Development (TDD)?
- Describe your “ideal” setup for web application. In your opinion how would today's
  “state-of-the-art” project should look like? Describe used technologies, development workflow,
  how would you approach testing, deployment, etc.

## Functional paradigm

- What are the specifics of Functional programming?
  - _pure functions, composition, no shared state, no side effects, immutability_
- What functional languages do you know? Are they purely functional?

## Linux

- Name some Linux distros. What are differences between them? Which is your favorite and why?
- What does SSH command do? When and how do you use it? Do you know any similar programs?
- What does "Linux File Permissions" means? What are they for? How can we set them and alter them?
- What are the differences between Windows and Linux file systems?

## Python

- What are the key features of Python? Why would you choose Python for new project? In what cases
  you would choose not to use Python?
- What are the differences between Python and some other languages?
- Python and multithreading. Is it a good idea? What are some ways to get some Python code to run
  in parallel?
- Do you know any package manager system for Python?
- Can you compile Python to executable file? When would you use that? What are the benefits and
  downsides?
- What Python frameworks for web development do you know? What are difference between them? Why
  would choose one over other?

## JavaScript

- What does ES6 means? Do you know at least 3 new features which it brings to JavaScript?
- What it is ECMAScript? How is it related to JavaScript?
- What is difference between `var`, `let` and `const`? Do you know what it is _variable hoisting_?
- What is the difference between `unassigned`, `undefined` and `null` variable?
- What it is transpiling? Why are we using it? Do you know any transpilers?
- What are polyfills and why are they needed?
- What it is code bundling? What bundlers do you know?

## NodeJS

- What it is NodeJS? How it is related to JavaScript? Why it is separate project?
- Why and when would you choose NodeJS over other solutions for new project? Why not?
- What it is V8 (engine)?
- What NodeJS frameworks for web development do you know? What are the differences between them?

## React

### Junior
- What is the difference between _state_ and _props_?
- What it means _composition vs inheritance_ and what are the practical consequences?
- Do you know Redux? Why would you use it?
- Why is immutability important in Redux? What are the drawbacks? How can we overcome them?

### Senior
- Why do we have both controlled and uncontrolled inputs?
- Why do we need a key property?
- What are _HOCs_, how would you create them and use them?
- What are synthetic events in React?
- What are React hooks? Any interesting properties?
- Describe Redux workflow.
- Describe asynchronous actions. What libraries do you know that works with them?

## DevOps/Deploy architecture

- What it is Docker? What problem is Docker trying to solve?
- What it is Kubernetes? How it is related to Swarm? What are the similarities and what are the
  differences?
- What are the advantages of deploying in Cloud? What are disadvantages? What common cloud
  services do you know?
- What relational databases do you know? Can you name few differences between them? What database
  would you choose for new project and why? Do you know any other types of database? What are
  their pros and cons?
- Can you describe vertical vs horizontal scaling of the database? Do you know what it is database
  sharding?

# How to run tests

Run scripts are platform idiomatic:

```
yarn crypto
pipenv run crypto
make crypto
```
