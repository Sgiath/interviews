defmodule Interviews.CoordinatesTest do
  @moduledoc """
  Tests for Coordinates exercise
  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  use ExUnit.Case, async: true

  alias Interviews.Coordinates.Exercise, as: Coordinates

  test "example" do
    assert Coordinates.solution([
             {1, 1},
             {1, 6},
             {8, 3},
             {3, 4},
             {5, 5},
             {8, 9}
           ]) == 17
  end
end
