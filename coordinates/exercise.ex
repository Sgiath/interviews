defmodule Interviews.Coordinates.Exercise do
  @moduledoc false

  @type point :: {integer(), integer()}

  @doc """
  Solve the problem

  Returns area of largest non-infinte point
  """
  @spec solution([point()]) :: integer()
  def solution(input) do
    input
  end

  @doc """
  Load text input
  """
  @spec load_input() :: [point()]
  def load_input do
    "coordinates/input.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Stream.map(&String.split(&1, ", ", trim: true, parts: 2))
    |> Stream.map(fn [x, y] -> {String.to_integer(x), String.to_integer(y)} end)
    |> Enum.to_list()
  end
end
