# Crypto

**Author:** Sgiath <FilipVavera@sgiath.dev>

**Difficulty**: Advanced

We can encrypt any text message given the encryption key:

    'a' -> '1'
    'b' -> '2'
    'c' -> '3'
     .
     .
     .
    'z' -> '26'

so message 'ab' will become '12'. But a decryption is not that strightforward since we
are not preserving spaces between letters so the example message can decoded as 'ab' or
as 'l' since 'l' is encrypted to '12' too.

Write a function which will compute how many ways encoded string can be decoded:

    num_ways('12') -> 2  # ('ab', 'l')

_Bonus:_ the final algorithm should have O(n) complexity
