"""Example solutions for Crypto exercise in Python

Author: Sgiath <FilipVavera@sgiath.dev>
"""
from functools import lru_cache
from typing import Callable, Dict, Optional


def naive(msg: str) -> int:
    """Naive approach with complexity O(2^n).

    Junior:
        It is good for junior to come up with this solution but he should recognize
        it is not good solution. He should be able to describe how it could be fixed
        even if he is not able to write it as code.

    Senior:
        Senior should come up with this solution pretty quickly and should recognize
        it is bad solution and should be able to fix it with dynamic programming.
    """
    # Empty string has exactly one way to decode it
    if msg == "":
        return 1

    # No letter code starts with the 0 so the message is invalid
    if msg[0] == "0":
        return 0

    # Every one digit code is decodable
    result = naive(msg[1:])

    # Every two digit code less or equal to 26 is decodable
    if len(msg) >= 2 and int(msg[:2]) <= 26:
        result += naive(msg[2:])

    return result


def basic_dynamic(msg: str) -> int:
    """Solution using dynamic programming in it's raw form.

    Junior:
        If he is able to come up with this solution it is really great.

    Senior:
        This is good solution showing that the person is able to understand
        the algorithmic solution for this problem but it is not "pythonic" solution
        so if he is not able to refactor it to use decorators it is not good sign of
        good Python developer. Decorator solution should be the first idea for
        the senior Python programmer.
    """
    return _basic_dynamic_helper(msg)


def _basic_dynamic_helper(msg: str, memo: Optional[Dict[str, int]] = None) -> int:
    # Set default value for memo
    if memo is None:
        memo = {"": 1}

    # No letter code starts with the 0 so the message is invalid
    if msg[0] == "0":
        return 0

    # Use memoized result if it exists
    if msg in memo:
        return memo[msg]

    # Every one digit code is decodable
    result = _basic_dynamic_helper(msg[1:], memo)

    # Every two digit code less or equal to 26 is decodable
    if len(msg) >= 2 and int(msg[:2]) <= 26:
        result += _basic_dynamic_helper(msg[2:], memo)

    # Save result to the memo
    memo[msg] = result

    return result


@lru_cache(maxsize=512)
def build_in_decorator_dynamic(msg: str) -> int:
    """Solution same as naive but using build-in lru_cache() decorator for memoizing results.

    Shows great knowledge of Python. If needed can be asked how lru_cache() works or
    to write it on your own (see memoize() function below).
    """
    # Empty string has exactly one way to decode it
    if msg == "":
        return 1

    # No letter code starts with the 0 so the message is invalid
    if msg[0] == "0":
        return 0

    # Every one digit code is decodable
    result = build_in_decorator_dynamic(msg[1:])

    # Every two digit code less or equal to 26 is decodable
    if len(msg) >= 2 and int(msg[:2]) <= 26:
        result += build_in_decorator_dynamic(msg[2:])

    return result


def memoize(func: Callable[[str], int]) -> Callable[[str], int]:
    """Memoize decorator for dynamic programming.

    Shows great knowledge of Python and dynamic programming. If this is first solution
    the question can be if he knows if there is something in standard library which do
    similar thing (see lru_cache() function).
    """
    memo: Dict[str, int] = {}

    def helper(msg: str) -> int:
        if msg not in memo:
            memo[msg] = func(msg)
        return memo[msg]

    return helper
