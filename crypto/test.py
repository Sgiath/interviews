"""Test crypto extercise

Author: Sgiath <FilipVavera@sgiath.dev>
"""
import unittest

from .exercise import num_ways


class TestNumWays(unittest.TestCase):
    """Test function num_ways."""

    def test_base_example(self) -> None:
        """Test example given in exercise description."""
        self.assertEqual(num_ways("12"), 2)

    def test_advance_example1(self) -> None:
        """Test some advance example."""
        self.assertEqual(num_ways("12345"), 3)

    def test_advance_example2(self) -> None:
        """Test some advance example."""
        self.assertEqual(num_ways("23456"), 2)

    def test_advance_example3(self) -> None:
        """Test some advance example."""
        self.assertEqual(num_ways("56789"), 1)

    def test_ones(self) -> None:
        """Test message consisting only with ones."""
        self.assertEqual(num_ways("111111"), 13)

    def test_with_starting_zero(self) -> None:
        """Test string starting with zero (invalid message)."""
        self.assertEqual(num_ways("01234"), 0)

    def test_with_correct_zero(self) -> None:
        """Test string with zero on correct place."""
        self.assertEqual(num_ways("12034"), 1)

    def test_with_incorrect_zero(self) -> None:
        """Test zero on incorect place."""
        self.assertEqual(num_ways("12304"), 0)

    def test_empty_string(self) -> None:
        """Test empty string."""
        self.assertEqual(num_ways(""), 1)


if __name__ == "__main__":
    unittest.main()
