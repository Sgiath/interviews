/**
 * Naive approach with O(2^n) complexity
 * 
 * **Author:** Sgiath <FilipVavera@sgiath.dev>
 */
export function naive(msg) {
  // Empty string can be interpreted only one way
  if (msg === "") {
    return 1;
  }

  // No code starts with zero -> invalid string
  if (msg[0] === "0") {
    return 0;
  }

  // One digit code is always valid so check the rest of the string
  let result = naive(msg.substring(1));

  // Two digit code is valid only if it is smaller or equal 26
  if (msg.length >= 2 && parseInt(msg.substring(0, 2)) <= 26) {
    result += naive(msg.substring(2));
  }

  return result;
}

/**
 * Approach using dynamic programming
 *
 * Pass already solved results as second parameter to recursive function
 */
export function dynamic(msg) {
  return dynamicHelper(msg);
}

/**
 * Using helper function to preserve given function parameters
 *
 * Initialize memo parameter to known results
 */
function dynamicHelper(msg, memo = { "": 1 }) {
  // No code starts with zero -> invalid string
  if (msg[0] === "0") {
    return 0;
  }

  // Check already computed results
  if (msg in memo) {
    return memo[msg];
  }

  // One digit code is always valid so check the rest of the string
  let result = dynamicHelper(msg.substring(1), memo);

  // Two digit code is valid only if it is smaller or equal 26
  if (msg.length >= 2 && parseInt(msg.substring(0, 2)) <= 26) {
    result += dynamicHelper(msg.substring(2), memo);
  }

  // Add computed result to memo parameter
  memo[msg] = result;

  return result;
}

/**
 * More universal memoized() function
 *
 * Construct memoized() wrapper function
 */
export const withMemoizedFunc = memoized(naive);

function memoized(func) {
  const memo = { "": 1 };

  return (msg) => {
    if (!memo.hasOwnProperty(msg)) {
      memo[msg] = func(msg);
    }
    return memo[msg];
  };
}
