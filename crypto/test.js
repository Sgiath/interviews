import { numWays } from "./exercise";

test("base example", () => {
  expect(numWays("12")).toBe(2);
});

test("advance example 1", () => {
  expect(numWays("12345")).toBe(3);
});

test("advance example 2", () => {
  expect(numWays("23456")).toBe(2);
});

test("advance example 3", () => {
  expect(numWays("56789")).toBe(1);
});

test("all ones", () => {
  expect(numWays("111111")).toBe(13);
});

test("with starting zero", () => {
  expect(numWays("01234")).toBe(0);
});

test("with correct zero", () => {
  expect(numWays("12034")).toBe(1);
});

test("with incorrect zero", () => {
  expect(numWays("12304")).toBe(0);
});

test("empty string", () => {
  expect(numWays("")).toBe(1);
});
