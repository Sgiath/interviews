"""Game of Life exercise solution

Author: Sgiath <FilipVavera@sgiath.dev>
"""
import itertools
from typing import Generator, List, Set, Tuple

Cell = Tuple[int, int]
Board = Set[Cell]


def neighbors(cell: Cell) -> Generator[Cell, None, None]:
    """Generate cell's neighbors."""
    x, y = cell
    yield x + 1, y
    yield x - 1, y
    yield x, y + 1
    yield x, y - 1
    yield x + 1, y + 1
    yield x + 1, y - 1
    yield x - 1, y + 1
    yield x - 1, y - 1


def advance(board: Board) -> Board:
    """Advance game one tick forward."""
    # new state that will be returned
    newstate = set()

    # compute all affected cells which needs to be recalculated
    # (union of original cells with all of theirs neighbors)
    recalc = board | set(itertools.chain(*map(neighbors, board)))

    # go through every affected cell
    for cell in recalc:
        # calculate number of live neighbors
        count = sum((neigh in board) for neigh in neighbors(cell))

        # if cell has 3 neighbors or two and is already alive
        if count == 3 or (count == 2 and cell in board):
            # is alive in next tick
            newstate.add(cell)
    return newstate


def game_of_life(seed: Board, ticks: int = 10) -> List[Board]:
    """Game of life implementation."""
    result = [seed]
    for _ in range(ticks):
        result.append(advance(result[-1]))
    return result
