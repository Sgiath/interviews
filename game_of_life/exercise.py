"""Game of Life exercise"""
from typing import List, Set, Tuple

Cell = Tuple[int, int]
Board = Set[Cell]


def game_of_life(seed: Board, ticks: int = 10) -> List[Board]:
    """Simulate Game of Life.

    Arguments:
        seed: initial board configuration
        ticks: number of iterations to simulate
    """
    # TODO: your implementation
    pass
